package den.hotel;

import java.util.Scanner;

public class HotelManagement {

    String floors,rooms,selectFloor,selectRoom,ch;
    int f,r,sf,sr;
    Scanner in = new Scanner(System.in);
    String[][] room;

    public HotelManagement(){

        //Set up floor and room
        setupHotel();


        //Choose menu
        do{
            System.out.println("-------- Welcome to Hotel Management System ---------");
            System.out.println("1- Check In");
            System.out.println("2- Checkout");
            System.out.println("3- Display");
            System.out.println("4- Exit");
            System.out.println("------------------------------------------");
            System.out.print("-> Choose option(1-4):");
            ch = in.next();
            switch (ch){
                case "1":
                    checkIn();
                    break;
                case "2":
                    checkOut();
                    break;
                case "3":
                    display();
                    break;
                case "4":
                    System.out.println("Bye bro...");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Option not valid");
            }
        }while (true);

    }


    void setupHotel(){
        System.out.println("------- Setting up hotel --------");
        do{
            System.out.print("-> Enter numbers of floors:");
            floors = in.next();
        }while (!Validation.isValidFloor(floors));
        do{
            System.out.print("-> Enter number of rooms in each floors:");
            rooms = in.next();
        }while (!Validation.isValidRoom(rooms));
        f = Integer.valueOf(floors);
        r = Integer.valueOf(rooms);
        System.out.println("=> Hotel is already setup with "+floors+" floors and "+rooms+" rooms successfully.");
        room = new String[f][r];
    }

    void checkIn(){
        System.out.println("------- Check in hotel -------");
        do{
            if(Integer.valueOf(floors)==1){
                System.out.print("-> Enter floor number(there only 1 floor):");
            }else {
                System.out.print("-> Enter floor number(1-"+floors+"):");
            }
            selectFloor = in.next();
        }while (!Validation.isValidFloor(selectFloor,Integer.valueOf(floors)));
        do{
            if(Integer.valueOf(rooms)==1){
                System.out.print("-> Enter room number(here only 1 room):");
            }else {
                System.out.print("-> Enter room number(1-"+rooms+"):");
            }
            selectRoom = in.next();
        }while (!Validation.isValidRoom(selectRoom,Integer.valueOf(rooms)));
        sf = Integer.valueOf(selectFloor)-1;
        sr = Integer.valueOf(selectRoom)-1;
        if(room[sf][sr]==null){
            in.nextLine();
            String name;
            do{
                System.out.print("-> Enter guest name:");
                name = in.nextLine();
                name = room[sf][sr] = name.replaceAll("\\s{2,}"," ");
            }while (!Validation.isValidName(name));
            System.out.println(name+" check in successfully!");
        }
        else{
            System.out.println("This room is already check in!");
        }
    }

    void checkOut(){
        do{
            if(f==1){
                System.out.print("-> Enter floor number(there only 1 floor):");
            }else {
                System.out.print("-> Enter floor number(1-"+floors+"):");
            }
            selectFloor = in.next();

        }while (!Validation.isValidFloor(selectFloor,Integer.valueOf(floors)));
        do{
            if(r==1){
                System.out.print("-> Enter room number(here only 1 room):");
            }else {
                System.out.print("-> Enter room number(1-"+rooms+"):");
            }
            selectRoom = in.next();
        }while (!Validation.isValidRoom(selectRoom,Integer.valueOf(rooms)));
        sf = Integer.valueOf(selectFloor)-1;
        sr = Integer.valueOf(selectRoom)-1;
        if(room[sf][sr]!=null){
            boolean b = true;
            do{
                System.out.println("Press 1 to checkout and 0 to cancel:");
                String verify = in.next();
                switch (verify){
                    case "1":{
                        System.out.println(room[sf][sr]+" has been check out successfully!");
                        room[sf][sr] = null;
                        b=false;
                        break;
                    }
                    case "0":{
                        System.out.println("You have cancel the checkout!");
                        b=false;
                        break;
                    }
                    default:{
                        System.out.println("Invalid input!");
                    }
                }
            }while (b);
        }
        else{
            System.out.println("No one check out this room you can't checkout!");
        }
    }

    void display(){
        System.out.print("          ");
        for(int i=0;i<r;i++){
            System.out.printf("%-20s","Room "+(i+1));
        }
        System.out.println();
        System.out.print("          ");
        for(int i=0;i<r;i++){
            System.out.printf("%-20s","------");
        }
        System.out.println();
        for(int i=0;i<room.length;i++){
            System.out.print("Floor "+(i+1)+" : ");
            for (int j=0;j<room[i].length;j++){
                System.out.printf("%-20s",room[i][j]);
            }
            System.out.println();
        }
    }

}
